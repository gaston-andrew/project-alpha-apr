from django.shortcuts import redirect
from projects.models import Project
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView


# Create your views here.


class ProjectList(LoginRequiredMixin, ListView):
    model = Project
    template_name = "projects/list.html"
    context_object_name = "list_projects"

    def get_queryset(self):
        return Project.objects.filter(members=self.request.user)


class ProjectDetails(DetailView):
    model = Project
    template_name = "projects/detail.html"
    context_object_name = "project_details"


class CreateProject(LoginRequiredMixin, CreateView):
    model = Project
    template_name = "projects/create.html"
    fields = ["name", "description", "members"]
    context_object_name = "project_creation"

    def form_valid(self, form):
        project = form.save(commit=False)
        project.owner = self.request.user
        project.save()
        return redirect("show_project", pk=project.id)
