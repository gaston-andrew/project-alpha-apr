from django.urls import path
from .views import ProjectList, ProjectDetails, CreateProject


urlpatterns = [
    path("", ProjectList.as_view(), name="list_projects"),
    path("<int:pk>/", ProjectDetails.as_view(), name="show_project"),
    path("create/", CreateProject.as_view(), name="create_project"),
]
