from django.urls import path
from .views import CreateTask, TaskList, UpdateTask

urlpatterns = [
    path("create/", CreateTask.as_view(), name="create_task"),
    path("mine/", TaskList.as_view(), name="show_my_tasks"),
]
