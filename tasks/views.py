from django.shortcuts import redirect
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView
from .models import Task
from django.contrib.auth.mixins import LoginRequiredMixin


# Create your views here.
class CreateTask(CreateView, LoginRequiredMixin):
    model = Task
    template_name = "tasks/create_task.html"
    fields = ["name", "start_date", "due_date", "project", "assignee"]
    context_object_name = "task_create"

    def form_valid(self, form):
        task = form.save(commit=False)
        task.owner = self.request.user
        task.save()
        return redirect("show_project", pk=task.id)


class TaskList(ListView, LoginRequiredMixin):
    model = Task
    template_name = "tasks/task_list.html"
    context_object_name = "tasks_list"


class UpdateTask(UpdateView):
    model = Task
    fields = ["is_completed"]
